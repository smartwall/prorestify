'use strict';

function create() {
    let controller = () => {
        return true;
    };

    return {
        controller
    };
}

module.exports = {
    create: create
};
