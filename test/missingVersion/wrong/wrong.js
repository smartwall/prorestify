'use strict';

function create() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function list() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function get() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function update() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function patch() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function del() {
  let controller = () => {
    return true;
  };

  return {
    controller
  };
}

function hire() {
  let controller = () => {
    return true;
  };

  return {
    item: {
      controller
    },
    collection: {
      controller
    }
  };
}

function getHomeAddress() {
  let controller = () => {
    return true;
  };

  return {
    item: {
      controller
    },
    collection: {
      controller
    }
  };
}

module.exports = {
  create: create,
  list: list,
  get: get,
  update: update,
  patch: patch,
  del: del,
  hire: hire,
  getHomeAddress: getHomeAddress,
};
